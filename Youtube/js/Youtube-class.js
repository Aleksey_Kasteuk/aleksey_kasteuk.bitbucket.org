function Youtube(size) {
	this.videos = [];
	this.windowSize = size;
	this.currentPage = 0;
	Array.observe(this.videos, function() {
		this.currentPage = 0;
		this.render();
		this.swipe(0);
	}.bind(this));
}

Youtube.prototype._getVideoStatistic = function(videos) {
	var ids = [];
	videos.forEach(function(value) {
		ids.push(value.id.videoId);
	});
	var request = new XMLHttpRequest();
	request.open("GET", "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + ids.join(",") + "&key=AIzaSyB7jsaLJ-8RLUb9txvXnTRYMTqTqDEmOg8");
	request.send();
	request.onreadystatechange = function() {
		if(request.readyState == 4) {
			if(request.status == 200) {
				var tmp = JSON.parse(request.responseText).items;
				var i;
				for(i = 0; i < tmp.length; i++) {
					videos[i].statistics = tmp[i].statistics;
				}
				this._getVideoChanels(videos);
			}
			else {
				console.log("Bad request");
			}
		}
	}.bind(this)
};

Youtube.prototype._getVideoChanels = function(videos) {
	var ids = [];
	videos.forEach(function(value) {
		ids.push(value.snippet.channelId);
	});
	var request = new XMLHttpRequest();
	request.open("GET", "https://www.googleapis.com/youtube/v3/channels?part=id%2C+snippet&id=" + ids.join(",") + "&key=AIzaSyB7jsaLJ-8RLUb9txvXnTRYMTqTqDEmOg8");
	request.send();
	request.onreadystatechange = function() {
		if(request.readyState == 4) {
			if(request.status == 200) {
				var tmp = JSON.parse(request.responseText).items;
				var i;
				for(i = 0; i < tmp.length; i++) {
					videos[i].channel = tmp[i].snippet;
				}
				this.videos.push(videos);
			}
			else {
				console.log("Bad request");
			}
		}
	}.bind(this)
};

Youtube.prototype.getVideos = function(searchName) {
 	var request = new XMLHttpRequest();
	request.open("GET", "https://www.googleapis.com/youtube/v3/search?part=id%2C+snippet&q=" + searchName + "&type=video&maxResults=15&key=AIzaSyB7jsaLJ-8RLUb9txvXnTRYMTqTqDEmOg8");
	request.setRequestHeader("Access-Control-Allow-Origin", "*");
	if(this.videos.length != 0) {
		this.videos.splice(0, this.videos.length);
	}
	request.send();
	request.onreadystatechange = function() {
		if(request.readyState == 4) {
			if(request.status == 200) {
				var response = JSON.parse(request.responseText).items;
				this._getVideoStatistic(response);
			}
			else {
				console.log("Bad request");
			}
		}
	}.bind(this)
 };

 Youtube.prototype.render = function() {
 	var videos = document.getElementById('videos');
 	videos.innerHTML = "";
 	document.getElementById('tooltip').innerHTML = "<div id='under-tooltip'></div>";
 	if(!this.videos[0]) {
 		return;
 	}
 	if(!this.videos[0][0]) {
 		return;
 	}
 	var i = 0;
 	var videosWraper;
 	videosWraper = document.createElement("div");
	videosWraper.classList.add('videos_wraper');
 	var video;
 	for(i; i < this.videos[0].length; i++) {
 		if(i % this.windowSize == 0) {
 			if(i != 0) {
 				videos.appendChild(videosWraper);
 				var element = document.createElement('div');
 				element.classList.add("tip");
 				element.setAttribute('value', Math.floor(i / this.windowSize) - 1);
 				document.getElementById('tooltip').appendChild(element);
 			}
			videosWraper = document.createElement("div");
			videosWraper.classList.add('videos_wraper');
 		}
 		video = document.createElement("div");
 		video.classList.add('video');
 		video.style.width = (100 / this.windowSize) + '%';
 		video.innerHTML =
 			"<a href='https://www.youtube.com/watch?v=" + this.videos[0][i].id.videoId + "'>" +
 			"<img src='" + this.videos[0][i].snippet.thumbnails.medium.url +
 			"' ondrag='return false' ondragdrop='return false' ondragstart='return false' unselectable='on' style='width: 100%;'>" +
 			"<h1>" + this.videos[0][i].snippet.title + "</h1>" +
 			"</a>" +
 			"<a href='https://www.youtube.com/channel/" + this.videos[0][i].snippet.channelId + "'>" +
 			"<div class='channel'><span class='icon-'>&#xf007</span> " + this.videos[0][i].channel.title + "</div>" + 
 			"</a>"+
 			"<h2><span class='icon-'>&#xf073</span> Published: " + new Date(this.videos[0][i].snippet.publishedAt).toDateString().split(' ').splice(1, 3).join(' ') + "</h2>" +
 			"<h3>Description:<br />" + this.videos[0][i].snippet.description + "</h3>" +
 			"<div class='statistics'>" +
 			"<div class='stat'><span class='icon-'>&#xf06e</span> " + this.videos[0][i].statistics.viewCount + "</div>" + 
 			"<div class='stat'><span class='icon-'>&#xf075</span> " + this.videos[0][i].statistics.commentCount + "</div>" + 
 			"<div class='stat'><span class='icon-'>&#xf164</span> " + this.videos[0][i].statistics.likeCount + "</div>" + 
 			"<div class='stat'><span class='icon-'>&#xf165</span> " + this.videos[0][i].statistics.dislikeCount + "</div>" + 
 			"</div>"
 		videosWraper.appendChild(video);
 	}
 	if(videosWraper) {
 		videos.appendChild(videosWraper);
		var element = document.createElement('div');
		element.classList.add("tip");
		var index = Math.floor(i / this.windowSize);
		index -= i % this.windowSize == 0 ? 1 : 0;
		element.setAttribute('value', index);
		element.addEventListener('click', function() {
			this.currentPage = element.getAttribute('value');
			this.swipe(0);
		}.bind(this));
		document.getElementById('tooltip').appendChild(element);
 	}
 	[].forEach.call(document.querySelectorAll('.tip'), function(v) {
 		v.addEventListener('click', function() {
			this.currentPage = v.getAttribute('value');
			this.swipe(0);
		}.bind(this));
		v.addEventListener('mouseenter', function(e) {
			var modern_media_query = window.matchMedia( "screen and (-webkit-min-device-pixel-ratio:2)");
			if(modern_media_query.matches) {
				return;
			}
			console.log(modern_media_query.matches);
			var tip = document.getElementById('under-tooltip');
			tip.innerText = v.getAttribute('value') - 0 + 1;
			tip.style.left = v.offsetLeft + "px";
			tip.style.display = 'block';
		});
		v.addEventListener('mouseout', function() {
			var modern_media_query = window.matchMedia( "screen and (-webkit-min-device-pixel-ratio:2)");
			if(modern_media_query.matches) {
				return;
			}
			var tip = document.getElementById('under-tooltip');
			tip.style.display = 'none';
		});
 	}.bind(this))
 };

 Youtube.prototype.resizeing = function(newSize) {
 	if(newSize == this.windowSize) {
 		return;
 	}
 	var currentVideo = this.windowSize * this.currentPage;
 	console.log(currentVideo);
 	this.windowSize = newSize;
 	if(this.windowSize > 0) {
 		this.currentPage = Math.floor(currentVideo / this.windowSize);
 	}
 	this.render();
 };

 Youtube.prototype.swipe = function (translation) {
 	if(Math.abs(translation) > 100) {
 		if(translation > 0) {
			this.currentPage--;
 		}
 		else {
 			this.currentPage++;
 		}
 	}
 	if(!this.videos[0]) {
 		return;
 	}
 	if(!this.videos[0][0]) {
 		return;
 	}
 	if(this.currentPage >= Math.floor(this.videos[0].length / this.windowSize) && this.videos[0].length % this.windowSize == 0) {
		this.currentPage = Math.floor(this.videos[0].length / this.windowSize) - 1;
	}
	if(this.currentPage > Math.floor(this.videos[0].length / this.windowSize)) {
		this.currentPage = Math.floor(this.videos[0].length / this.windowSize);
	}
	if(this.currentPage < 0) {
		this.currentPage = 0;
	}
	document.getElementById('videos').style.transition = "0.2s";
 	var translate = document.getElementById('videos').offsetWidth * -this.currentPage - (10 * this.currentPage);
 	translate = document.getElementById('videos').offsetWidth * -this.currentPage - (10 * this.currentPage);
 	document.getElementById('videos').style.webkitTransform = "translateX(" + translate + "px)";
 	var tips = document.querySelectorAll('.tip');
 	var i = 0;
 	[].forEach.call(tips, function(v) {
 		if(i == this.currentPage) {
 			v.classList.add('tip_checked');
 		}
 		else {
 			if(v.classList.contains('tip_checked')) {
 				v.classList.remove('tip_checked');
 			}
 		}
 		i++;
 	}.bind(this));
 }