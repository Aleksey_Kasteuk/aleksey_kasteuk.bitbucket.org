function createTemplate() {
	document.querySelector('body').innerHTML = 
	"<header>\
		<a href='http://youtube.com/' id='logo'><img src='img/YouTube-logo-full_color.png' alt=''></a>\
		<div id='search'>\
			<input type='text' id='search_place' placeholder='search'>\
			<label for='search_place'></label>\
		</div>\
	</header>\
	<section id='main'>\
		<section id='videos'></section>\
	</section>\
	<footer>\
		<article id='tooltip'></article>\
	</footer>";
}