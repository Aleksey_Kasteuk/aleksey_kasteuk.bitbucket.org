function Youtube(size) {
	this.videos = [];
	this.windowSize = size;
	Array.observe(this.videos, function() {
		console.log(this.windowSize, this.videos);
		this.render();
	}.bind(this));
}

Youtube.prototype._getVideoStatistic = function(videos) {
	var ids = [];
	videos.forEach(function(value) {
		ids.push(value.id.videoId);
	});
	var request = new XMLHttpRequest();
	request.open("GET", "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + ids.join(",") + "&key=AIzaSyB7jsaLJ-8RLUb9txvXnTRYMTqTqDEmOg8");
	request.send();
	request.onreadystatechange = function() {
		if(request.readyState == 4) {
			if(request.status == 200) {
				var tmp = JSON.parse(request.responseText).items;
				var i;
				for(i = 0; i < tmp.length; i++) {
					videos[i].statistics = tmp[i].statistics;
				}
				this.videos.push(videos);
			}
			else {
				console.log("Bad request");
			}
		}
	}.bind(this)
};

Youtube.prototype.getVideos = function(searchName) {
 	var request = new XMLHttpRequest();
	request.open("GET", "https://www.googleapis.com/youtube/v3/search?part=id%2C+snippet&q=" + searchName + "&maxResults=15&key=AIzaSyB7jsaLJ-8RLUb9txvXnTRYMTqTqDEmOg8");
	request.setRequestHeader("Access-Control-Allow-Origin", "*");
	if(this.videos.length != 0) {
		this.videos.splice(0, this.videos.length);
	}
	request.send();
	request.onreadystatechange = function() {
		if(request.readyState == 4) {
			if(request.status == 200) {
				var response = JSON.parse(request.responseText).items;
				this._getVideoStatistic(response);
			}
			else {
				console.log("Bad request");
			}
		}
	}.bind(this)
 };

 // Youtube.prototype.render = function() {
 // 	if(!this.videos[0]) {
 // 		return;
 // 	}
 // 	if(!this.videos[0][0]) {
 // 		return;
 // 	}
 // 	var i = 0;
 // 	var video;
 // 	var videos = document.getElementById('videos');
 // 	var margins = Math.floor(this.videos[0].length / this.windowSize);
 // 	videos.innerHTML = "";
 // 	videos.style.width = (100 * this.videos[0].length / this.windowSize + margins * 2) + "%";
 // 	for(i; i < this.videos[0].length; i++) {
 // 		video = document.createElement("div");
 // 		video.classList.add('video');
 // 		if(i % this.windowSize == 0 && i != 0) {
 // 			video.style.marginLeft =  "10px";
 // 		}
 // 		video.style.width = (100 / (this.videos[0].length)) + '%';
 // 		videos.appendChild(video);
 // 	}
 // 	console.log(100 / (this.videos[0].length));
 // };

 Youtube.prototype.render = function() {
 	if(!this.videos[0]) {
 		return;
 	}
 	if(!this.videos[0][0]) {
 		return;
 	}
 	var i = 0;
 	var videosWraper;
 	var video;
 	var videos = document.getElementById('videos');
 	videos.innerHTML = "";
 	for(i; i < this.videos[0].length; i++) {
 		if(i % this.windowSize == 0) {
 			if(i != 0) {
 				videos.appendChild(videosWraper);
 			}
			videosWraper = document.createElement("div");
			videosWraper.classList.add('videos_wraper');
 		}
 		video = document.createElement("div");
 		video.classList.add('video');
 		video.style.width = (100 / this.windowSize) + '%';
 		videosWraper.appendChild(video);
 	}
 	if(videosWraper) {
 		videos.appendChild(videosWraper);
 	}
 };

 Youtube.prototype.resizeing = function(newSize) {
 	if(newSize == this.windowSize) {
 		return;
 	}
 	this.windowSize = newSize;
 	this.render();
 };