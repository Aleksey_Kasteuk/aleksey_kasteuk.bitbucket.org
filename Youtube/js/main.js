var youtube;
var translate = 0;
var currentX1, currentX2;

window.onload =  function(){
	createTemplate();
	var width = document.getElementById('main').offsetWidth;
	youtube = new Youtube(Math.floor(width / 300));
	document.querySelector("label").addEventListener('click', function() {
		youtube.getVideos(document.querySelector("input").value);
	});
	document.querySelector("input").addEventListener('keypress', function(e) {
		if(e.keyCode == 13) {
			youtube.getVideos(this.value);
		}
	});
	document.getElementById('videos').addEventListener('mousedown', function(e) {
		currentX1 = e.clientX;
		currentX2 = e.clientX;
	});
	document.getElementById('videos').addEventListener('mousemove', function(e) {
		if(!currentX1) {
			return;
		}
		currentX2 = e.clientX;
 		var translate = document.getElementById('videos').offsetWidth * -youtube.currentPage + currentX2 - currentX1;
 		document.getElementById('videos').style.webkitTransform = "translateX(" + translate + "px)"
	});
	document.getElementById('videos').addEventListener('mouseup', function() {
		youtube.swipe(currentX2 - currentX1);
		currentX1 = undefined;
		currentX2 = undefined;
	});
	document.getElementById('videos').addEventListener('touchstart', function(e) {
		if(e.touches.length != 1) {
			return;
		}
		currentX1 = e.changedTouches[0].pageX;
		currentX2 = e.changedTouches[0].pageX;
	});
	document.getElementById('videos').addEventListener('touchmove', function(e) {
		if(!currentX1) {
			return;
		}
		if(e.touches.length != 1) {
			youtube.swipe(currentX2 - currentX1);
			currentX1 = undefined;
			currentX2 = undefined;
			return;
		}
		currentX2 = e.changedTouches[0].pageX;
 		var translate = document.getElementById('videos').offsetWidth * -youtube.currentPage + currentX2 - currentX1;
 		document.getElementById('videos').style.webkitTransform = "translateX(" + translate + "px)"
	});
	document.getElementById('videos').addEventListener('touchend', function(e) {
		if(!currentX1) {
			return;
		}
		e.preventDefault();
		youtube.swipe(currentX2 - currentX1);
		currentX1 = undefined;
		currentX2 = undefined;
	});
	document.getElementById('videos').addEventListener('transitionend', function() {
		this.style.transition = null;
	});
};

window.addEventListener('resize', function() {
	if(!youtube) {
		return;
	}
	var width = document.getElementById('main').offsetWidth;
	youtube.resizeing(Math.floor(width / 300));
	youtube.swipe(0);
});